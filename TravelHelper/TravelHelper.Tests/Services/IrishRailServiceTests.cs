﻿using NUnit.Framework;
using System;
using TravelHelper.Services;
using TravelHelper.Models;
using Moq;
using System.Threading.Tasks;

namespace TravelHelper.Tests
{
    [TestFixture]
    public class IrishRailServiceTests
    {
        [SetUp]
        public void SetUp()
        {
            _mockedRest = new Mock<IRestService>();
            _service = new IrishRailService(_mockedRest.Object);
        }

        private IIrishRailService _service;
        private Mock<IRestService> _mockedRest;
        private string _stationsResource = "/realtime/realtime.asmx/getAllStationsXML";
        private string _trainsResource = "/realtime/realtime.asmx/getCurrentTrainsXML";

        [Test]
        public async Task GetAllStations_ReturnsNeededObject_True()
        {
            // Arrange
            _mockedRest.Setup(x => x.GetAsync<ArrayOfObjStation>(_stationsResource)).Returns(Task.FromResult(new ArrayOfObjStation()));
            // Act
            var res = await _service.GetAllStations();
            // Assert
            Assert.IsInstanceOf<ArrayOfObjStation>(res);
        }

        [Test]
        public async Task GetCurrentTrains_ReturnsNeededObject_True()
        {
            // Arrange
            _mockedRest.Setup(x => x.GetAsync<ArrayOfObjTrainPositions>(_trainsResource)).Returns(Task.FromResult(new ArrayOfObjTrainPositions()));
            // Act
            var res = await _service.GetCurrentTrains();
            // Assert
            Assert.IsInstanceOf<ArrayOfObjTrainPositions>(res);
        }

        [Test]
        public async Task GetTrainMovements_ReturnsNeededObject_True()
        {
            // Arrange
            var id = "e109";
            var date = new DateTime(2011, 12, 21);
            var dateAsString = date.ToString("dd MMM yyyy");
            var encodedDate = Uri.EscapeDataString(dateAsString);
            var url = $"/realtime/realtime.asmx/getTrainMovementsXML?TrainId={id}&TrainDate={encodedDate}";

            _mockedRest.Setup(x => x.GetAsync<ArrayOfObjTrainMovements>(url)).Returns(Task.FromResult(new ArrayOfObjTrainMovements()));
            // Act
            var res = await _service.GetTrainMovements(id, date);
            // Assert
            Assert.IsInstanceOf<ArrayOfObjTrainMovements>(res);
        }

    }
}

