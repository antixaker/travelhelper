﻿using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("objTrainPositions")]
    public class ObjTrainPositions
    {
        [XmlElement("TrainStatus")]
        public string TrainStatus { get; set; }

        [XmlElement("TrainLatitude")]
        public string TrainLatitude { get; set; }

        [XmlElement("TrainLongitude")]
        public string TrainLongitude { get; set; }

        [XmlElement("TrainCode")]
        public string TrainCode { get; set; }

        [XmlElement("TrainDate")]
        public string TrainDate { get; set; }

        [XmlElement("PublicMessage")]
        public string PublicMessage { get; set; }

        [XmlElement("Direction")]
        public string Direction { get; set; }
    }

}