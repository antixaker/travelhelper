﻿using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("objStation")]
    public class ObjStation
    {
        [XmlElement("StationDesc")]
        public string StationDesc { get; set; }

        [XmlElement("StationAlias")]
        public string StationAlias { get; set; }

        [XmlElement("StationLatitude")]
        public string StationLatitude { get; set; }

        [XmlElement("StationLongitude")]
        public string StationLongitude { get; set; }

        [XmlElement("StationCode")]
        public string StationCode { get; set; }

        [XmlElement("StationId")]
        public string StationId { get; set; }
    }
}