﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("ArrayOfObjStation")]
    public class ArrayOfObjStation
    {
        [XmlElement("objStation")]
        public List<ObjStation> ObjStation { get; set; }
    }
}

