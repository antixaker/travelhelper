﻿using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("objTrainMovements")]
    public class ObjTrainMovements
    {
        [XmlElement("TrainCode")]
        public string TrainCode { get; set; }

        [XmlElement("TrainDate")]
        public string TrainDate { get; set; }

        [XmlElement("LocationCode")]
        public string LocationCode { get; set; }

        [XmlElement("LocationFullName")]
        public string LocationFullName { get; set; }

        [XmlElement("LocationOrder")]
        public string LocationOrder { get; set; }

        [XmlElement("LocationType")]
        public string LocationType { get; set; }

        [XmlElement("TrainOrigin")]
        public string TrainOrigin { get; set; }

        [XmlElement("TrainDestination")]
        public string TrainDestination { get; set; }

        [XmlElement("ScheduledArrival")]
        public string ScheduledArrival { get; set; }

        [XmlElement("ScheduledDeparture")]
        public string ScheduledDeparture { get; set; }

        [XmlElement("ExpectedArrival")]
        public string ExpectedArrival { get; set; }

        [XmlElement("ExpectedDeparture")]
        public string ExpectedDeparture { get; set; }

        [XmlElement("Arrival")]
        public string Arrival { get; set; }

        [XmlElement("Departure")]
        public string Departure { get; set; }

        [XmlElement("AutoArrival")]
        public string AutoArrival { get; set; }

        [XmlElement("AutoDepart")]
        public string AutoDepart { get; set; }

        [XmlElement("StopType")]
        public string StopType { get; set; }
    }
}