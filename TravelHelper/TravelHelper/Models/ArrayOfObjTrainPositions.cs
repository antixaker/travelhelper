﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("ArrayOfObjTrainPositions")]
    public class ArrayOfObjTrainPositions
    {
        [XmlElement("objTrainPositions")]
        public List<ObjTrainPositions> ObjTrainPositions { get; set; }
    }
}

