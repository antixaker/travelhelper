﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TravelHelper.Models
{
    [XmlRoot("ArrayOfObjTrainMovements")]
    public class ArrayOfObjTrainMovements
    {
        [XmlElement("objTrainMovements")]
        public List<ObjTrainMovements> ObjTrainMovements { get; set; }
    }
}

