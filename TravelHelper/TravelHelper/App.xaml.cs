﻿using System;
using Xamarin.Forms;
using FreshMvvm;
using TravelHelper.Services;

namespace TravelHelper
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            RegisterServices();
            MainPage = new TravelHelperPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private void RegisterServices()
        {
            FreshIOC.Container.Register<IIrishRailService, IrishRailService>();
            FreshIOC.Container.Register<IRestService, RestService>();
        }
    }
}

