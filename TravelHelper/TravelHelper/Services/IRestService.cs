﻿using System;
using System.Threading.Tasks;

namespace TravelHelper.Services
{
    public interface IRestService
    {
        Task<T> GetAsync<T>(string resource);

        string BaseUrl { get; set; }

        string DefaultNamespace { get; set; }
    }
}

