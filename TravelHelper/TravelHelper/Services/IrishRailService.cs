﻿using System;
using System.Threading.Tasks;
using TravelHelper.Models;

namespace TravelHelper.Services
{
    public class IrishRailService : IIrishRailService
    {
        private IRestService _restService;

        public IrishRailService(IRestService restService)
        {
            _restService = restService;
            _restService.BaseUrl = "http://api.irishrail.ie";
            _restService.DefaultNamespace = "http://api.irishrail.ie/realtime/";
        }

        public Task<ArrayOfObjStation> GetAllStations()
        {
            return _restService.GetAsync<ArrayOfObjStation>("/realtime/realtime.asmx/getAllStationsXML");
        }

        public Task<ArrayOfObjTrainPositions> GetCurrentTrains()
        {
            return _restService.GetAsync<ArrayOfObjTrainPositions>("/realtime/realtime.asmx/getCurrentTrainsXML");
        }

        public Task<ArrayOfObjTrainMovements> GetTrainMovements(string id, DateTime date)
        {
            var url = GetUrlString(id, date);
            return _restService.GetAsync<ArrayOfObjTrainMovements>(url);
        }

        private string GetUrlString(string id, DateTime date)
        {
            var dateAsString = date.ToString("dd MMM yyyy");
            var encodedDate = Uri.EscapeDataString(dateAsString);
            var url = $"/realtime/realtime.asmx/getTrainMovementsXML?TrainId={id}&TrainDate={encodedDate}";
            return url;
        }
    }
}

