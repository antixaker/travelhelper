﻿using System;
using TravelHelper.Models;
using System.Threading.Tasks;

namespace TravelHelper.Services
{
    public interface IIrishRailService
    {
        Task<ArrayOfObjStation> GetAllStations();

        Task<ArrayOfObjTrainPositions> GetCurrentTrains();

        Task<ArrayOfObjTrainMovements> GetTrainMovements(string id, DateTime date);
    }
}

