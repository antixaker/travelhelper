﻿using System;
using System.Threading.Tasks;
using System.Net.Http;
using System.Text;
using System.IO;
using System.Xml.Serialization;

namespace TravelHelper.Services
{
    public class RestService : IRestService
    {
        public string BaseUrl { get; set; }

        public string DefaultNamespace { get; set; }

        public async Task<T> GetAsync<T>(string resource)
        {
            using (var client = new HttpClient())
            {
                using (var response = await client.GetAsync(CreateRequestUrl(BaseUrl, resource)).ConfigureAwait(false))
                {
                    ThrowIfNotSuccess(response);

                    var data = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

                    var buffer = Encoding.UTF8.GetBytes(data);

                    using (var stream = new MemoryStream(buffer))
                    {
                        var serializer = new XmlSerializer(typeof(T), DefaultNamespace);

                        return (T)serializer.Deserialize(stream);
                    }
                }
            }
        }

        private string CreateRequestUrl(string baseUrl, string resource)
        {
            return baseUrl + resource;
        }

        private void ThrowIfNotSuccess(HttpResponseMessage response)
        {
            if (!response.IsSuccessStatusCode)
            {
                response.EnsureSuccessStatusCode();
            }
        }
    }
}

